package com.calling.peek;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.waheedsabir.hclient_projectauto_call_answer.R;

public class Settings extends AppCompatActivity {

    Switch speaker_for_all,answer_for_all,Mute_Profile,notification;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        sharedPreferences=getSharedPreferences(Constant.spref,MODE_PRIVATE);
        editor=sharedPreferences.edit();

        speaker_for_all=findViewById(R.id.allow);
        answer_for_all=findViewById(R.id.answers);
        Mute_Profile=findViewById(R.id.mute);
        notification=findViewById(R.id.notification);

        getSupportActionBar().setTitle("Settings");

        if (getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if(sharedPreferences.getBoolean(Constant.speaker_for_all,true)){
            speaker_for_all.setChecked(true);

        }

        if(sharedPreferences.getBoolean(Constant.answer_for_all,true)){
            answer_for_all.setChecked(true);
        }
        if(sharedPreferences.getBoolean(Constant.Mute_Profile,true)){
            Mute_Profile.setChecked(true);
        }
        if(sharedPreferences.getBoolean(Constant.notification,true)){
            notification.setChecked(true);
        }

        speaker_for_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean(Constant.speaker_for_all,true);
                    editor.commit();
                }
                else {
                    editor.putBoolean(Constant.speaker_for_all,false);
                    editor.commit();
                }
            }
        });

        answer_for_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean(Constant.answer_for_all,true);
                    editor.commit();
                }
                else {

                    editor.putBoolean(Constant.answer_for_all,false);
                    editor.commit();
                }
            }
        });
        Mute_Profile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean(Constant.Mute_Profile,true);
                    editor.commit();
                }
                else {
                    editor.putBoolean(Constant.Mute_Profile,false);
                    editor.commit();
                }
            }
        });
        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean(Constant.notification,true);
                    editor.commit();
                }
                else {
                    editor.putBoolean(Constant.notification,false);
                    editor.commit();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
