package com.calling.peek.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.calling.peek.Adapter.Call_History_Adapter;
import com.calling.peek.Adapter.History_GetSet;
import com.calling.peek.Constant;
import com.calling.peek.Database;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Call_History extends Fragment {

    Database db=null;

    Context context;
    SharedPreferences sharedPreferences;
    ArrayList<History_GetSet> itemslist;

    ListView listView;
    public boolean isfarmentcreate=false;

    public Call_History() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_call_history, container, false);
       context=getContext();
       db = new Database(context);
       sharedPreferences=context.getSharedPreferences(Constant.spref,Context.MODE_PRIVATE);
       listView=v.findViewById(R.id.history_list);

      isfarmentcreate=true;
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(isfarmentcreate)
            loaddata();
        }
    }

    public void loaddata(){
        final Cursor cr=db.gethistory(sharedPreferences.getString("user_id","00"));

        if(cr.getCount()==0){

        }
        else {
            itemslist=new ArrayList<>();
            cr.moveToFirst();
            for (int i = 1; i <= cr.getCount(); i++) {
                History_GetSet history_getSet=new History_GetSet();
                history_getSet.setMyid(cr.getString(cr.getColumnIndex("myid")));
                history_getSet.setUserid(cr.getString(cr.getColumnIndex("userid")));
                history_getSet.setUsername(cr.getString(cr.getColumnIndex("username")));
                history_getSet.setUserimage(cr.getString(cr.getColumnIndex("userimage")));
                history_getSet.setCalltype(cr.getString(cr.getColumnIndex("calltype")));
                history_getSet.setCalltime(cr.getString(cr.getColumnIndex("calltime")));
                history_getSet.setCallingtime(cr.getString(cr.getColumnIndex("callingtime")));

                itemslist.add(history_getSet);
                cr.moveToNext();
            }
            Call_History_Adapter adapter=new Call_History_Adapter(context,R.layout.item_call__history,itemslist);
            listView.setAdapter(adapter);
        }
    }

}
