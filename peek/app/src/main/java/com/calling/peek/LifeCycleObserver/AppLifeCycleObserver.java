package com.calling.peek.LifeCycleObserver;


/*
 * TODO Step 2: Create a AppLifeCycleObserver that extends LifeCycleObserver.
 *
 * When app enters foreground, the receiver is registered. And when app goes into
 * background the receiver is unregistered.
 * */

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.IntentFilter;

import com.calling.peek.Calling.ShowCallStatus;

public class AppLifeCycleObserver implements LifecycleObserver{

    private Context mContext;
    private ShowCallStatus receiver;

    public AppLifeCycleObserver(Context context) {
        mContext = context;
    }

    /**
     * TODO Step 3: Create onEnterForeground method to register the receiver
     * When app enters foreground
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onEnterForeground() {
        // init the receiver
        receiver = new ShowCallStatus();

        // define intent filter
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ShowCallStatus.INTENT_ACTION);

        // register the receiver using mContext
        mContext.registerReceiver(receiver, intentFilter);
    }

    /**
     * TODO Step 4: Create onEnterBackground method to unregister the receiver
     * When app enters background
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onEnterBackground() {
        // unregister the receiver
        //mContext.unregisterReceiver(receiver);
    }
}
