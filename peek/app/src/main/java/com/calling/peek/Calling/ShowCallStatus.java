package com.calling.peek.Calling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by AQEEL on 4/29/2018.
 */

public class ShowCallStatus extends BroadcastReceiver {

    public static final String INTENT_ACTION = "callBroadcat";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("commingcall==", "cam");
        Intent intent2 = new Intent(context,Calling.class);
        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent2.putExtra("receipt_id",intent.getStringExtra("receipt_id"));
        intent2.putExtra("connectstatus",intent.getStringExtra("connectstatus"));
        intent2.putExtra("Buttonstatus",intent.getStringExtra("Buttonstatus"));
        context.startActivity(intent2);
    }
}
