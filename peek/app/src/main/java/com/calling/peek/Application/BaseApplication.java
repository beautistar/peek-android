package com.calling.peek.Application;


/*
 * TODO Step 5: Create BaseApplication class
 * */

import android.app.Application;
import android.arch.lifecycle.ProcessLifecycleOwner;

import com.calling.peek.LifeCycleObserver.AppLifeCycleObserver;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // TODO Step 6: Init lifeCycleObserver using application context
        AppLifeCycleObserver lifeCycleObserver
                = new AppLifeCycleObserver(getApplicationContext());

        // TODO Step 7: Add life cycle observer to ProcessLifecycleOwner
        ProcessLifecycleOwner.get()
                .getLifecycle()
                .addObserver(lifeCycleObserver);
    }
}
