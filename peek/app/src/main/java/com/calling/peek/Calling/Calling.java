package com.calling.peek.Calling;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Constant;
import com.calling.peek.Database;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Calling extends AppCompatActivity implements ServiceCallback, SensorEventListener {
    CallingService mService;
    boolean mBound = false;

    private String recipientId;
    Intent i;

    TextView username;
    ImageView userimage;
    SharedPreferences sharedPreferences;
    String myid;

    RelativeLayout receive_disconnect_layout;

    ImageButton receive_call,disconnect_call,cancel_call;


    private TextView callState,timer;

    LinearLayout speakermiclayout;
    ImageButton micbtn,speakerbtn;


    AudioManager audioManager ;


    Database db;
    public static boolean iscalling=false;
    public String uname,uimage;


    SimpleDateFormat df = new SimpleDateFormat("28-MMM hh:mm a");
    Calendar calender;


    private SensorManager mSensorManager;
    private Sensor mProximity;



    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private int field = 0x00000020;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.calling);


        //todo waheedsabir popup
        getSupportActionBar().hide();
        DisplayMetrics displayMetrics = new DisplayMetrics() ;
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels ;
        getWindow().setLayout((int )(width*1), (int )(height*1));

        //todo waheedsabir notification
        /*NotificationChannel channel = null;   // for heads-up notifications
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel("channel01", "name",
                    NotificationManager.IMPORTANCE_HIGH);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel.setDescription("description");
        }

// Register channel with system
        NotificationManager notificationManager = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notificationManager = getSystemService(NotificationManager.class);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(channel);
        }
        Notification notification = new NotificationCompat.Builder(this, "channel01")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Test")
                .setContentText("You see me!")

                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)   // heads-up
                .build();

        NotificationManagerCompat notificationManager_q = NotificationManagerCompat.from(this);
        notificationManager_q.notify(0, notification);*/



        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_CALL);


        sharedPreferences=getSharedPreferences(Constant.spref, MODE_PRIVATE);
        myid=sharedPreferences.getString("user_id","");

        db=new Database(this);

        receive_disconnect_layout=findViewById(R.id.receive_disconnect_layout);

        receive_call=findViewById(R.id.receivecall);
        disconnect_call=findViewById(R.id.disconnect_call);
        cancel_call=findViewById(R.id.cancel_call);

        speakermiclayout=findViewById(R.id.speakermiclayout);

        micbtn=findViewById(R.id.micbtn);
        speakerbtn=findViewById(R.id.speakerbtn);

        username=findViewById(R.id.username);
        userimage=findViewById(R.id.userimage);

        callState = (TextView)findViewById(R.id.callState);
        timer=findViewById(R.id.timer);

       i=getIntent();
        if(i!=null){


            recipientId=i.getStringExtra("receipt_id");
            Log.d("callig","working" +recipientId);
                //Send notification for running serviecs
            notification_call(recipientId);


            getuserdata(recipientId);
            if(i.getStringExtra("connectstatus")!=null){
                if(i.getStringExtra("connectstatus").equals("call_ended")){
                    finishCall();
                    return;
                }
                callState.setText(""+i.getStringExtra("connectstatus"));
            }
            if(i.getStringExtra("Buttonstatus")!=null){
                if(i.getStringExtra("Buttonstatus").equals("Answer")){
                    receive_disconnect_layout.setVisibility(View.VISIBLE);
                    audioManager.setSpeakerphoneOn(true);
                }else {
                    cancel_call.setVisibility(View.VISIBLE);
                }
            }

        }

        receive_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.answercall();
            }
        });

        disconnect_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.EndCall();
                finish();
            }
        });

        cancel_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishCall();

            }
        });


        //this code is about light turning off
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        try {
            // Yeah, this is hidden field.
            field = PowerManager.class.getClass().getField("PROXIMITY_SCREEN_OFF_WAKE_LOCK").getInt(null);
        } catch (Throwable ignored) {
        }

        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(field, getLocalClassName());

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        //todo waheedsabir
    this.registerReceiver(this.NetworkBroadCast, filter);
    }


    public void finishCall() {

        if (mService != null) {
            mService.EndCall();
        }

        finish();
    }

    public void ShowTime(String time){
        timer.setText(time);
    }

    public void checkmic(){

      //  Toast.makeText(getApplicationContext(), ""+myid+recipientId, Toast.LENGTH_SHORT).show();


        if(!sharedPreferences.getBoolean(Constant.Mute_Profile,true)){
        if(sharedPreferences.getBoolean(myid+recipientId+"mic",true)){
            audioManager.setMicrophoneMute(false);
            micbtn.setImageResource(R.drawable.ic_mic_blue);
        }else {
            audioManager.setMicrophoneMute(true);
            micbtn.setImageResource(R.drawable.mic);
        }
        }else {
            audioManager.setMicrophoneMute(true);
            micbtn.setImageResource(R.drawable.mic);
        }
        micbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioManager.isMicrophoneMute()) {
                    micbtn.setImageResource(R.drawable.ic_mic_blue);
                    audioManager.setMicrophoneMute(false);
                }else {
                    audioManager.setMicrophoneMute(true);
                    micbtn.setImageResource(R.drawable.mic);
                }
            }
        });
    }

    public void checkspeaker(){
        if (sharedPreferences.getBoolean(Constant.speaker_for_all,true)) {
            if(sharedPreferences.getBoolean(myid+recipientId+"speaker",true))
            {
                speakerbtn.setImageResource(R.drawable.speaker);
                audioManager.setSpeakerphoneOn(true);
            }
            else {
                audioManager.setSpeakerphoneOn(false);
                speakerbtn.setImageResource(R.drawable.ic_speaker_off);
            }
        }else {
            audioManager.setSpeakerphoneOn(false);
            speakerbtn.setImageResource(R.drawable.ic_speaker_off);
        }
        speakerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioManager.isSpeakerphoneOn() == false) {
                    speakerbtn.setImageResource(R.drawable.speaker);
                    audioManager.setSpeakerphoneOn(true);
                }else {
                    audioManager.setSpeakerphoneOn(false);
                    speakerbtn.setImageResource(R.drawable.ic_speaker_off);
                }
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, CallingService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mService.setCallbacks(null);
            unbindService(mConnection);
            mBound = false;
        }
    }


    @Override
    public void ShowStatus(String status) {
        if(status.equals("Hang Up")){

        }
        else if(status.equals("Call")) {
            if(iscalling){
                calender=Calendar.getInstance();

                db.addhistory(sharedPreferences.getString("user_id","00"),
                        recipientId,uname,uimage,"receive",timer.getText().toString()
                ,df.format(calender.getTime()));
                iscalling=false;
            }

            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);

            finish();
        }
        else if(status.equals("Connected")){
            callState.setText("connected");
            iscalling=true;
            receive_disconnect_layout.setVisibility(View.GONE);
            cancel_call.setVisibility(View.VISIBLE);
            speakermiclayout.setVisibility(View.VISIBLE);

            checkmic();
            checkspeaker();

            if(sharedPreferences.getBoolean(Constant.speaker_for_all,false)){
            }else {
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            }

        }
        else if(status.equals("ringing")){
            callState.setText("ringing");
        }

        else if(status.equals("Answer")){
            callState.setText("Calling You");
            receive_disconnect_layout.setVisibility(View.VISIBLE);
            cancel_call.setVisibility(View.GONE);
        }

    }


    public void getuserdata(final String userid){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.getuserbyID,
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response) {
                        try {
                            String change;
                            JSONObject jsonObject=new JSONObject(response);
                            JSONObject jsonObject1=jsonObject.getJSONObject("user_model");
                            username.setText(""+jsonObject1.getString("user_name"));
                           uname=""+jsonObject1.getString("user_name");


                            change =   jsonObject1.getString("photo_url");

                            if(change.equals("")){
                                Picasso.get()
                                        .load(R.drawable.placeholder)
                                        .placeholder(R.drawable.placeholder)
                                        .error(R.drawable.placeholder)
                                        .into(userimage);
                            }else {
                                Picasso.get()
                                        .load(change)
                                        .placeholder(R.drawable.placeholder)
                                        .error(R.drawable.placeholder)
                                        .into(userimage);

                            }
                            uimage=change;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", userid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(Calling.this);
        requestQueue.add(postRequest);

    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            CallingService.LocalBinder binder = (CallingService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.setCallbacks(Calling.this);

            if(i.getStringExtra("Buttonstatus")!=null){

            }else {
                if(!CallingService.iscallrunning){
            mService.Do_call(recipientId);
                }
            cancel_call.setVisibility(View.VISIBLE);
        }

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    //this code is all about light turning on off
    @Override
    protected void onResume() {
// Register a listener for the sensor.
        super.onResume();
        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
// Be sure to unregister the sensor when the activity pauses.
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    boolean isclose=true;
    boolean isopen=true;
    @Override
    public final void onSensorChanged(SensorEvent event) {
        int x = (int) event.values[0];
       if(x<1){
           if (isclose) {
               if(!wakeLock.isHeld()) {
               wakeLock.acquire();
           }

               isclose=false;
           }
           isopen=true;
       }else {
           if(isopen) {
               if(wakeLock.isHeld()) {
               wakeLock.release();
               }
               isopen=false;
           }
           isclose=true;
           }
       }

    @Override
    public void onBackPressed() {
          if(CallingService.iscallrunning){
              Toast.makeText(this, "Call is Running", Toast.LENGTH_SHORT).show();
              }
            else {
              finish();
            }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {

            mService.EndCall();
        }
    }

    private BroadcastReceiver NetworkBroadCast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(!haveNetworkConnection()){
                mService.EndCall();

            }
        }
    };

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    private void notification_call(final String recipientId) {

        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.POST, Constant.Send_Notification,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("registerToken response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "Network issue");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getString("user_id",null));
                params.put("target_id", recipientId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }

    
}

