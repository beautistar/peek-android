package com.calling.peek;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Calling.CallingService;
import com.waheedsabir.hclient_projectauto_call_answer.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    EditText email,password;
    String strEmail, strPassword;
    //  Button btn_login, btn_register;

    ProgressDialog dialog;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(Constant.spref,MODE_PRIVATE);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.pass);

        strEmail = sharedPreferences.getString("email","");
        strPassword = sharedPreferences.getString("password","");
        email.setText(strEmail);
        password.setText(strPassword);

        dialog = new ProgressDialog(this);

        if (!strEmail.equals("") && !strPassword.equals("")) {
            dialog.setMessage("Sign in ...");
            dialog.show();
            doLogin();
        }

        if (ContextCompat.checkSelfPermission(
                MainActivity.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED )
        {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE},
                    1);
        }
    }

    public void forget(View view) {

        startActivity(new Intent(MainActivity.this , Forget.class));

    }

    public void menu(View view) {

        dialog.setMessage("Sign in ...");
        dialog.show();

        strEmail = email.getText().toString().trim();
        strPassword = password.getText().toString().trim();
        doLogin();

    }

    private void doLogin() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.URL_Login,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject =new JSONObject(response);

                            String message =   jsonObject.getString("result_code");


                            if(message.equals("200")){
                                //these code for token register


                                SharedPreferences.Editor editor= sharedPreferences.edit();
                                editor.putString("user_id",jsonObject.getJSONObject("user_model").getString("user_id"));
                                editor.putString("email",jsonObject.getJSONObject("user_model").getString("email"));
                                editor.putString("user_name",jsonObject.getJSONObject("user_model").getString("user_name"));
                                editor.putString("phone_number",jsonObject.getJSONObject("user_model").getString("phone_number"));
                                editor.putString("photo_url",jsonObject.getJSONObject("user_model").getString("photo_url"));
                                editor.putString("password",strPassword);
                                editor.putBoolean("islogin",true);
                                editor.commit();


                                /// Call registerToken API

                                dialog.dismiss();
                                registerToken();

                                if(isServiceRunning()){
                                    Intent service = new Intent(getApplicationContext(), CallingService.class);
                                    getApplicationContext().stopService(service);
                                    startService(service);

                                    startopposervices();
                                }else {
                                    Intent service = new Intent(getApplicationContext(), CallingService.class);
                                    startService(service);
                                }

                                startActivity(new Intent(MainActivity.this, Menu_main.class));
                                finish();

                            }else{
                                dialog.dismiss();
                                Toast.makeText(MainActivity.this, "Invalid Detail", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "Network issue");
                        Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("email", strEmail);
                params.put("password", strPassword);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }

    private void registerToken() {

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.URL_registerToken,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("registerToken response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "Network issue");
                        Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getString("user_id", ""));
                params.put("token", sharedPreferences.getString("fcm_token", ""));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }


    public void signup(View view) {
        startActivity(new Intent(MainActivity.this , SignUp.class));
    }


    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (Constant.Servicename.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void startopposervices() {

        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            }

            List<ResolveInfo> list = getApplicationContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if  (list.size() > 0) {
                getApplicationContext().startActivity(intent);
            }
        } catch (Exception e) {
           Log.d("error",""+e);
        }
    }
}