package com.calling.peek.fire_base;

import android.content.SharedPreferences;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import java.util.HashMap;
import java.util.Map;



/**
 * Created by filipp on 5/23/2016.
 */
public class FirebaseInstanceIDService extends FirebaseInstanceIdService {
    SharedPreferences sharedPreferences ;

    @Override
    public void onTokenRefresh() {
        sharedPreferences = getSharedPreferences(Constant.spref,MODE_PRIVATE);
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("Token", token);
        if (token.equals("")) {
            return;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit().putString("fcm_token", token);
        editor.commit();
        //Preference.getInstance().put(this, PrefConst.FCM_TOKEN,token);
        if (sharedPreferences.getString("user_id",null) == null) {
            return;
        }
        registerToken(token);
    }

    private void registerToken(final String token) {

        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.POST, Constant.URL_registerToken,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("registerToken response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "Network issue");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getString("user_id",null));
                params.put("token", token);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }
}
