package com.calling.peek.Adapter;

/**
 * Created by AQEEL on 5/6/2018.
 */

public class History_GetSet {
String myid,userid,username,userimage,calltype,calltime,callingtime;

    public String getMyid() {
        return myid;
    }

    public void setMyid(String myid) {
        this.myid = myid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserimage() {
        return userimage;
    }

    public void setUserimage(String userimage) {
        this.userimage = userimage;
    }

    public String getCalltype() {
        return calltype;
    }

    public void setCalltype(String calltype) {
        this.calltype = calltype;
    }

    public String getCalltime() {
        return calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }

    public String getCallingtime() {
        return callingtime;
    }

    public void setCallingtime(String callingtime) {
        this.callingtime = callingtime;
    }
}
