package com.calling.peek.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Adapter.FavoriteList_Adapter;
import com.calling.peek.Adapter.ListItem;
import com.calling.peek.Calling.Calling;
import com.calling.peek.Constant;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Favorite extends Fragment {


    RecyclerView.Adapter adapter ;
    private List<ListItem> listItems;

    SharedPreferences sharedPreferences ;

    RecyclerView recyclerView ;
    public static boolean isfavoriteAdd=false;

    public Favorite() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_favorite, container, false);

        sharedPreferences=getContext().getSharedPreferences(Constant.spref, Context.MODE_PRIVATE);

        recyclerView  = v.findViewById(R.id.favoritelist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        load();

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(isfavoriteAdd){
                load();
                isfavoriteAdd=false;
            }

        }
    }

    public void load (){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.favoritelist,
                new Response.Listener<String>()
                {

                    String change ;
                    @Override
                    public void onResponse(String response) {
                        Log.d("resp",response);
                        listItems = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("favorite_list");

                            for (int i = 0 ; i<jsonObject.getJSONArray("favorite_list").length();i++){


                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    change =   jsonObject1.getString("photo_url");

                                    sharedPreferences.edit()
                                            .putBoolean(jsonObject1.getString("user_id")+"favorite",true).commit();
                                        ListItem list_items = new ListItem(
                                        jsonObject1.getString("user_id"),
                                        jsonObject1.getString("email"),
                                        jsonObject1.getString("user_name"),
                                        jsonObject1.getString("phone_number"),
                                        change
                                );


                                listItems.add(list_items);

                            }

                            adapter= new FavoriteList_Adapter(listItems, getContext(), new FavoriteList_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(ListItem item) {
                                    Intent intent=new Intent(getContext(), Calling.class);
                                    intent.putExtra("receipt_id",item.getUser_id());
                                    getContext().startActivity(intent);
                                }

                                @Override
                                public void ReloadData() {
                                    load();
                                }
                            });

                            recyclerView.setAdapter(adapter);




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(getActivity(), "Network problem", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getString("user_id","0"));


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);
    }


}
