package com.calling.peek;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class ResetDialog extends AppCompatDialogFragment {


    private android.widget.EditText newpassword;
    private android.widget.EditText confirm;
    private android.widget.EditText reset;
    private android.widget.EditText et_code;

    private  DialogListener listener ;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dialog,null);

        this.et_code = (EditText) view.findViewById(R.id.code);
        this.reset = (EditText) view.findViewById(R.id.reset);
        this.confirm = (EditText) view.findViewById(R.id.confirm);
        this.newpassword = (EditText) view.findViewById(R.id.newpassword);

        builder.setView(view)
                .setTitle("Reset Password")


                .setNegativeButton("cencel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }
                }).setPositiveButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String code = et_code.getText().toString();
                String recoverypassword = reset.getText().toString();
                String newPasword = confirm.getText().toString();

                if (recoverypassword.equals(newPasword)){

                    SharedPreferences sh = getActivity().getSharedPreferences(Constant.spref,MODE_PRIVATE);

                    reset_password(sh.getString("reset_email",null), newPasword, code);

                }else {

                    Toast.makeText(getActivity(), "password not match ", Toast.LENGTH_SHORT).show();
                }


              //  Toast.makeText(getActivity(), ""+sh.getString("reset_email",null), Toast.LENGTH_SHORT).show();

              //  listener.applyText(recoverypassword, newPasword);
            }
        });
        return builder.create();

    }

    private void reset_password(final String reset_email, final String newPasword, final String code) {


        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.resetpass,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        try {

                            JSONObject jsonObject =new JSONObject(response);

                            //   String s =jsonObject.getJSONObject("user_model").getString("email");
                            String message =   jsonObject.getString("result_code");
                            if(message.equals("200")){

                               Log.d("reset","Password Reset ");


                            }else{
                                Toast.makeText(getActivity(), "Password Not Reset", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Log.d("Response", response);
                        //  Toast.makeText(MainActivity.this, ""+response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                        Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email",reset_email);
                params.put("password", newPasword);
                params.put("code", code);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {

            listener = (DialogListener) context;
        } catch (ClassCastException e) {
          throw  new ClassCastException(context.toString()+"must be implement ");
        }
    }

    public interface DialogListener {
        void applyText(String recovery , String newPassword);


    }
}
