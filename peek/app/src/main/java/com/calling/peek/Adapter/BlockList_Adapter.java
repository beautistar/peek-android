package com.calling.peek.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Block_List;
import com.calling.peek.Constant;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AQEEL on 5/4/2018.
 */

public class BlockList_Adapter extends RecyclerView.Adapter<BlockList_Adapter.My_holder> {
    private List<ListItem> listItems ;
    private Context context ;

    private BlockList_Adapter.OnItemClickListener listener;

    SharedPreferences sharedPreferences;

    public interface OnItemClickListener {
        void onItemClick(ListItem item);
    }

    public BlockList_Adapter(List<ListItem> listItems, Context context,BlockList_Adapter.OnItemClickListener listener) {
        this.listItems =listItems ;
        this.context =context ;
        this.listener=listener;
        sharedPreferences=context.getSharedPreferences(Constant.spref,Context.MODE_PRIVATE);
    }


    @NonNull
    @Override
    public BlockList_Adapter.My_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.block__list,parent,false);

        return new BlockList_Adapter.My_holder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull final BlockList_Adapter.My_holder holder, int position) {

        final ListItem listItem =listItems.get(position);
        holder.username.setText(listItem.user_name);
        holder.bind(listItems.get(position), listener);
        if(listItem.photo_url.equals("")){
            Picasso.get()
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.userimage);
        }else {
            Picasso.get()
                    .load(listItem.photo_url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.userimage);

        }
        holder.unblockbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unblockuser(sharedPreferences.getString("user_id","000"),listItem.getUser_id());

            }
        });

    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class My_holder extends RecyclerView.ViewHolder{


        public TextView username ;
        public ImageView userimage ;
        public ImageButton unblockbtn;

        public My_holder(View itemView) {
            super(itemView);

            username =itemView.findViewById(R.id.username);
            userimage =itemView.findViewById(R.id.userimage);
            unblockbtn=itemView.findViewById(R.id.unblockbtn);

        }
        public void bind(final ListItem item, final BlockList_Adapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public void unblockuser(final String senderid, final String receiverid){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.unblockuser,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("req",response);
                        sharedPreferences.edit().remove(receiverid).commit();
                        Block_List activity = (Block_List) context;
                        activity.load();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", senderid);
                params.put("target_id", receiverid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(postRequest);

    }


}

