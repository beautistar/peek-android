package com.calling.peek.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Block_List;
import com.calling.peek.Constant;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by AQEEL on 5/4/2018.
 */

public class SearchContactList_Adapter extends ArrayAdapter<ListItem> {


    private ArrayList<ListItem> resultList;

    SharedPreferences sharedPreferences;

    Context context;
    public SearchContactList_Adapter(Context context, int textViewResourceId, ArrayList<ListItem> items) {
        super(context, textViewResourceId, items);
        this.resultList=items;
        this.context=context;
        sharedPreferences=context.getSharedPreferences(Constant.spref, Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public ListItem getItem(int index) {
        return resultList.get(index);
    }


    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_all_user_list, parent, false);
        }

        TextView strName = (TextView) view.findViewById(R.id.name);
        ImageView imageView=view.findViewById(R.id.userimage);
        final Button button=view.findViewById(R.id.invitebtn);
        button.setBackgroundColor(context.getResources().getColor(R.color.red));
        button.setText("Block");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Blockuser(sharedPreferences.getString("user_id","000"),getItem(position).getUser_id());
                button.setText("Blocked");
            }
        });
        strName.setText(getItem(position).getUser_name());

        if(getItem(position).getPhoto_url().equals("")){
            Picasso.get()
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(imageView);
        }else {
            Picasso.get()
                    .load(getItem(position).getPhoto_url())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(imageView);

        }

        return view;
    }


    public void Blockuser(final String senderid, final String receiverid){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.blockuser,
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response) {

                        Log.d("req",response);
                        Block_List activity = (Block_List) context;
                        activity.load();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);

                        Block_List activity = (Block_List) context;
                        activity.load();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", senderid);
                params.put("target_id", receiverid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(postRequest);

    }


}