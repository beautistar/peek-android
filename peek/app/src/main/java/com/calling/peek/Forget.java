package com.calling.peek;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Forget extends AppCompatActivity  implements ResetDialog.DialogListener{


    private ImageView image;
    private EditText editText1;
    private android.widget.Button submit;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget);

        this.submit = (Button) findViewById(R.id.submit);
        this.editText1 = (EditText) findViewById(R.id.editText1);
        this.image = (ImageView) findViewById(R.id.image);
        dialog = new ProgressDialog(this);
    }

    public void submit(View view) {



        dialog.setMessage("Please Wait...");
        dialog.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.Forgetemail,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        try {

                            JSONObject jsonObject =new JSONObject(response);

                            //   String s =jsonObject.getJSONObject("user_model").getString("email");
                            String message =   jsonObject.getString("result_code");
                            if(message.equals("200")){
                                dialog.dismiss();
                              //  Toast.makeText(Forget.this, "Email Send Successful", Toast.LENGTH_SHORT).show();
                                    ResetDialog resetDialog = new ResetDialog();
                                    resetDialog.show(getSupportFragmentManager(),"Reset Password");
                              //  startActivity(new Intent(Forget.this, MainActivity.class));
                             //   finish();
                                SharedPreferences sharedPreferences = getSharedPreferences(Constant.spref,MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("reset_email",editText1.getText().toString());
                                editor.apply();


                            }else{
                                dialog.dismiss();
                                Toast.makeText(Forget.this, "Email not exist", Toast.LENGTH_SHORT).show();

                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        Log.d("Response", response);
                        //  Toast.makeText(MainActivity.this, ""+response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                        Toast.makeText(Forget.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", editText1.getText().toString());



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);

        //startActivity(new Intent(MainActivity.this , Menu_main.class));


    }

    @Override
    public void applyText(String recovery, String newPassword) {
     Toast.makeText(this, "Password Reset", Toast.LENGTH_SHORT).show();
    }
}
