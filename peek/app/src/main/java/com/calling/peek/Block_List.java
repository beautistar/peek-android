package com.calling.peek;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Adapter.BlockList_Adapter;
import com.calling.peek.Adapter.ListItem;
import com.calling.peek.Adapter.SearchContactList_Adapter;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Block_List extends AppCompatActivity{


    RecyclerView.Adapter adapter ;
    private ArrayList<ListItem> blockeditems;
    private ArrayList<ListItem> contactlist;
    private ArrayList<ListItem> filtercontactlist;

    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;

    RecyclerView recyclerView ;


    AutoCompleteTextView autoCompView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blocklist_activity);

        sharedPreferences=getSharedPreferences(Constant.spref, Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();

        recyclerView  =findViewById(R.id.blocklist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));

        findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        autoCompView = (AutoCompleteTextView) findViewById(R.id.searchlocation);

        Point pointSize = new Point();
        getWindowManager().getDefaultDisplay().getSize(pointSize);
        autoCompView.setDropDownWidth(pointSize.x);

        autoCompView.setDropDownBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        filtercontactlist=new ArrayList<>();


        load();

        findViewById(R.id.search_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetFilterlist();
            }
        });

    }

    public void load (){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.blocklist,
                new Response.Listener<String>()
                {

                    String change ;
                    @Override
                    public void onResponse(String response) {
                        Log.d("resp",response);
                        blockeditems = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("block_list");

                            for (int i = 0 ; i<jsonObject.getJSONArray("block_list").length();i++){


                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                                    change =   jsonObject1.getString("photo_url");


                                editor.putBoolean(jsonObject1.getString("user_id"),true).commit();
                                com.calling.peek.Adapter.ListItem list_items = new com.calling.peek.Adapter.ListItem(
                                        jsonObject1.getString("user_id"),
                                        jsonObject1.getString("email"),
                                        jsonObject1.getString("user_name"),
                                        jsonObject1.getString("phone_number"),
                                        change
                                );


                                blockeditems.add(list_items);

                            }

                            adapter= new BlockList_Adapter(blockeditems, Block_List.this, new BlockList_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(com.calling.peek.Adapter.ListItem item) {
                                }
                            });

                            recyclerView.setAdapter(adapter);
                            getcotectlit();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                        Toast.makeText(Block_List.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getString("user_id","0"));


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }



    public void getcotectlit (){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.Contectlist,
                new Response.Listener<String>()
                {

                    String change ;
                    @Override
                    public void onResponse(String response) {
                        Log.d("resp",response);
                        contactlist = new ArrayList<>();
                        try {
                            JSONObject  jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("contact_list");

                            for (int i = 0 ; i<jsonObject.getJSONArray("contact_list").length();i++){


                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    change =   jsonObject1.getString("photo_url");

                                if(jsonObject1.getString("status").equals("Accepted")) {
                                    ListItem list_items = new ListItem(
                                            jsonObject1.getString("user_id"),
                                            jsonObject1.getString("email"),
                                            jsonObject1.getString("user_name"),
                                            jsonObject1.getString("phone_number"),
                                            change,
                                            jsonObject1.getString("status")
                                    );


                                    contactlist.add(list_items);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                        Toast.makeText(Block_List.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getString("user_id","0"));


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue =Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }



    public void GetFilterlist(){
    filtercontactlist.clear();
    for (int i=0;i<contactlist.size();i++){
        if(contactlist.get(i).getUser_name().toLowerCase().contains(autoCompView.getText().toString().toLowerCase())){
            filtercontactlist.add(contactlist.get(i));
        }
    }
    SearchContactList_Adapter searchContactList_adapter=new SearchContactList_Adapter(Block_List.this,R.layout.item_all_user_list,filtercontactlist);
    autoCompView.setAdapter(searchContactList_adapter);
    autoCompView.showDropDown();
    }


}
