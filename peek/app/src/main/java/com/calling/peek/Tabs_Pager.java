package com.calling.peek;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.calling.peek.fragments.Call_History;
import com.calling.peek.fragments.Favorite;
import com.calling.peek.fragments.People;

public class Tabs_Pager extends FragmentStatePagerAdapter{
Context context;

    String[] titles = new String[]{"People" ,"Favorite","Call history"};

    public Tabs_Pager(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Fragment getItem(int position) {


        switch (position){
            case 2 :

                Call_History call_history = new Call_History() ;
                return call_history ;
            case 1 :
                Favorite favorite = new Favorite() ;
                return favorite ;
            case 0 :
                People people = new People() ;
                return people ;



        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
