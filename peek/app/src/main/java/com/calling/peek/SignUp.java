package com.calling.peek;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.RelateToUploadimage.AppHelper;
import com.calling.peek.RelateToUploadimage.VolleyMultipartRequest;
import com.calling.peek.RelateToUploadimage.VolleySingleton;
import com.waheedsabir.hclient_projectauto_call_answer.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUp extends AppCompatActivity {

    private de.hdodenhof.circleimageview.CircleImageView image;
    private android.widget.ImageButton imagebtn;
    private android.widget.EditText editText1;
    private android.widget.EditText pass;
    private android.widget.EditText phone;
    private android.widget.EditText username;

    public static final String user_name="user_name";
    public static final String email="email";
    public static final String phone_number="phone_number";
    public static final String password="password";
    String userid;
    ProgressDialog dialog;
    public boolean isimageselect=false;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        sharedPreferences = getSharedPreferences(Constant.spref,MODE_PRIVATE);
        this.phone = (EditText) findViewById(R.id.phone);
        this.pass = (EditText) findViewById(R.id.pass);
        this.editText1 = (EditText) findViewById(R.id.editText1);
        this.image = (CircleImageView) findViewById(R.id.image);
        this.username = (EditText) findViewById(R.id.username);

       this.imagebtn=(ImageButton)findViewById(R.id.upload_btn);
       this.imagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });

        dialog = new ProgressDialog(this);

    }

    public void signup(View view) {

        if ( !isimageselect ||phone.getText().toString().equals("")|| pass.getText().toString().equals("") ||editText1.getText().toString().equals("") || username.getText().toString().equals("")){

            Toast.makeText(this, "Fill the form complete ", Toast.LENGTH_SHORT).show();

        } else {
            dialog.setMessage("Sign up ...");
            dialog.show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.URL_Signup,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            //   String s =jsonObject.getJSONObject("user_model").getString("email");
                            String message = jsonObject.getString("result_code");
                            if (message.equals("200")) {
                                userid = jsonObject.getString("user_id");
                                SaveImageinDb(userid);
                            } else {
                                dialog.dismiss();
                                Toast.makeText(SignUp.this, "Email already exist", Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                        Toast.makeText(SignUp.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", editText1.getText().toString());
                params.put("user_name", username.getText().toString());
                params.put("phone_number", phone.getText().toString());
                params.put("password", pass.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }

    }



    private void SaveImageinDb(final String userid) {

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.uploadphoto, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Log.d("resp",response.toString());
                Toast.makeText(SignUp.this, "Sign Up Successfully", Toast.LENGTH_SHORT).show();
                registerToken();
                dialog.dismiss();
                startActivity(new Intent(SignUp.this,MainActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("file", new DataPart("image.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), image.getDrawable()), "image/jpeg"));

                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private void registerToken() {

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.URL_registerToken,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("registerToken response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "Network issue");
                        Toast.makeText(SignUp.this, "Network Issue", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("user_id", userid);
                params.put("token", params.put("token", sharedPreferences.getString("fcm_token", "")));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {

                Uri selectedImage = data.getData();

                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                final Bitmap imagebitmap = BitmapFactory.decodeStream(imageStream);
                Bitmap resized = Bitmap.createScaledBitmap(imagebitmap, 300, 300, true);
                image.setImageBitmap(resized);
                isimageselect=true;

            }
        }
    }
}
