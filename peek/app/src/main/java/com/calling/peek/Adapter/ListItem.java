package com.calling.peek.Adapter;

public class ListItem {

    String user_id ;
    String  email ;
    String  user_name ;
    String  phone_number ;
    String  photo_url ;
    String status;

    public ListItem(String user_id, String email, String user_name, String phone_number, String photo_url) {
        this.user_id = user_id;
        this.email = email;
        this.user_name = user_name;
        this.phone_number = phone_number;
        this.photo_url = photo_url;
    }

    public ListItem(String user_id, String email, String user_name, String phone_number, String photo_url, String status) {
        this.user_id = user_id;
        this.email = email;
        this.user_name = user_name;
        this.phone_number = phone_number;
        this.photo_url = photo_url;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getEmail() {
        return email;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getPhoto_url() {
        return photo_url;
    }
}
