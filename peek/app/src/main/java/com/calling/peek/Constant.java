package com.calling.peek;

public class Constant {

    public static String URL_Signup ="http://13.126.68.181/app/api/signup";
    public static String URL_Login ="http://13.126.68.181/app/api/signin";
    public static String Forgetemail ="http://13.126.68.181/app/api/forgotPassword";
    public static String resetpass ="http://13.126.68.181/app/api/resetPassword";
    public static String searchByName ="http://13.126.68.181/app/api/searchByName";

    public static String Contectlist="http://13.126.68.181/app/api/getContactList";

    public static String Servicename="com.calling.peek.Calling.CallingService";
    public static String SendInvite="http://13.126.68.181/app/api/sendInvite";
    public static String AcceptInvite="http://13.126.68.181/app/api/acceptInvite";


    public static String addfavorite="http://13.126.68.181/app/api/addFavorite";
    public static String favoritelist="http://13.126.68.181/app/api/getFavoriteList";
    public static String blockuser="http://13.126.68.181/app/api/blockUser";
    public static String blocklist="http://13.126.68.181/app/api/getBlockList";
    public static String unblockuser="http://13.126.68.181/app/api/unBlockUser";

    public static String URL_registerToken ="http://13.126.68.181/app/api/registerToken";
    public static String Send_Notification ="http://13.126.68.181/app/api/sendNotification";

    public static String getuserbyID="http://13.126.68.181/app/api/getUserById";

    public static String uploadphoto="http://13.126.68.181/app/api/uploadPhoto";
    public static String remove_favorite="http://13.126.68.181/app/api/removeFavorite";


    public static String spref="peek";
    public static String speaker_for_all="speaker_for_all";
    public static String answer_for_all="answer_for_all";
    public static String Mute_Profile="Mute_Profile";
    public static String notification="notification";



}
