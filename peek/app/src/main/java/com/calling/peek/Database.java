package com.calling.peek;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by AQEEL on 5/6/2018.
 */

public class Database extends SQLiteOpenHelper{

    public static final int DB_VERSION = 3;
    public static final String DB_NAME = "peekdata.db";
    Context contex;
    SQLiteDatabase sq = null;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        contex = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS History");
        onCreate(db);
    }

    public void createTable(SQLiteDatabase db) {
        String table = "create table History (id integer  primary key autoincrement, myid TEXT, userid TEXT, username TEXT, userimage TEXT, calltype TEXT, calltime TEXT, callingtime TEXT)";
          try {
              db.execSQL(table);
              } catch (Exception ex) {
        }
    }


    public long addhistory(String myid,String userid,String username,
                           String userimage,String calltype,String calltime
    ,String callingtime) {
        try {
            sq = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            sq.beginTransaction();
            cv.put("myid",myid);
            cv.put("userid", userid);
            cv.put("username", username);
            cv.put("userimage", userimage);
            cv.put("calltype",calltype);
            cv.put("calltime",calltime);
            cv.put("callingtime",callingtime);
            long result = sq.insert("History", null, cv);
            sq.setTransactionSuccessful();
            sq.endTransaction();
            return result;
        } catch (Exception ex) {
            Toast.makeText(contex, "Error in adding time", Toast.LENGTH_SHORT).show();
            return -1;
        }
    }


    public Cursor gethistory(String myid){
        sq=this.getReadableDatabase();
        Cursor cr=sq.rawQuery("select * from History WHERE myid = '"+myid+"' ORDER BY id DESC" ,null);
        return cr;
    }

}
