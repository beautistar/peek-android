package com.calling.peek.Calling;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.calling.peek.Constant;

/**
 * Created by AQEEL on 5/1/2018.
 */

public class ServiceStart_BroadCast extends BroadcastReceiver {
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        if(!isServiceRunning()){
            context.startService(new Intent(context, CallingService.class));
        }
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (Constant.Servicename.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
