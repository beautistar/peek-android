package com.calling.peek.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.calling.peek.Calling.Calling;
import com.calling.peek.Constant;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.ArrayList;

/**
 * Created by AQEEL on 5/6/2018.
 */

public class Call_History_Adapter extends ArrayAdapter<History_GetSet> {


    private ArrayList<History_GetSet> resultList;

    SharedPreferences sharedPreferences;
    Context context;


    public Call_History_Adapter(Context context, int item_call__history, ArrayList<History_GetSet> itemslist) {
        super(context,item_call__history,itemslist);
        this.resultList=itemslist;
        this.context=context;
        sharedPreferences=context.getSharedPreferences(Constant.spref, Context.MODE_PRIVATE);

    }


    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public History_GetSet getItem(int index) {
        return resultList.get(index);
    }


    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_call__history, parent, false);
        }
        TextView strName = (TextView) view.findViewById(R.id.username);
        ImageView imageView=view.findViewById(R.id.userimage);
        TextView calltime=view.findViewById(R.id.calltime);
        TextView callingtime=view.findViewById(R.id.callingtime);
        ImageButton calltype=view.findViewById(R.id.calltype);
        final ImageButton button=view.findViewById(R.id.callbtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Calling.class);
                intent.putExtra("receipt_id",getItem(position).getUserid());
                context.startActivity(intent);
            }
        });
        strName.setText(getItem(position).getUsername());
        if(getItem(position).getUserimage().equals("")){
            Picasso.get()
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(imageView);
        }else {
            Picasso.get()
                    .load(getItem(position).getUserimage())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(imageView);

        }

        calltime.setText(getItem(position).getCalltime());
        if(!getItem(position).getCalltype().equals("receive")){
            imageView.setImageResource(R.drawable.ic_outgoing_call);
        }
        callingtime.setText(getItem(position).getCallingtime());

        return view;
    }

}