package com.calling.peek.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Constant;
import com.calling.peek.fragments.People;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AQEEL on 5/4/2018.
 */

public class FavoriteList_Adapter  extends RecyclerView.Adapter<FavoriteList_Adapter.My_holder> {
    private List<ListItem> listItems ;
    private Context context ;

    private FavoriteList_Adapter.OnItemClickListener listener;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String myid;

    public interface OnItemClickListener {
        void onItemClick(ListItem item);
        void ReloadData();
    }

    public FavoriteList_Adapter(List<ListItem> listItems, Context context,FavoriteList_Adapter.OnItemClickListener listener) {
        this.listItems =listItems ;
        this.context =context ;
        this.listener=listener;
        sharedPreferences=context.getSharedPreferences(Constant.spref,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        myid=sharedPreferences.getString("user_id","");
    }


    @NonNull
    @Override
    public FavoriteList_Adapter.My_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_grid,parent,false);

        return new FavoriteList_Adapter.My_holder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteList_Adapter.My_holder holder, int position) {


        final ListItem listItem =listItems.get(position);
        holder.username.setText(listItem.user_name);
        holder.bind(listItems.get(position), listener);
        if(listItem.photo_url.equals("")){
            Picasso.get()
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.userimage);
        }else {
            Picasso.get()
                    .load(listItem.photo_url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.userimage);

        }

        holder.option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   Toast.makeText(context, ""+myid+listItem.getUser_id(), Toast.LENGTH_SHORT).show();

                PopupMenu popup = new PopupMenu(context, v);
                popup.inflate(R.menu.option_favprite);
                popup.show();
                if(!sharedPreferences.getBoolean(myid+listItem.getUser_id()+"mic",true)) {
                    popup.getMenu().getItem(0).setChecked(false);
                }
                if(!sharedPreferences.getBoolean(myid+listItem.getUser_id()+"speaker",true)) {
                    popup.getMenu().getItem(1).setChecked(false);
                }

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                int id = item.getItemId();
                                //noinspection SimplifiableIfStatement
                                if (id == R.id.mic) {
                                    if(sharedPreferences.getBoolean(myid+listItem.getUser_id()+"mic",true)){
                                        editor.putBoolean(myid+listItem.getUser_id()+"mic",false);
                                        editor.commit();
                                        Toast.makeText(context, "Mic Mute", Toast.LENGTH_SHORT).show();
                                    }else {
                                        editor.putBoolean(myid+listItem.getUser_id()+"mic",true);
                                        editor.commit();
                                        Toast.makeText(context, "Mic on", Toast.LENGTH_SHORT).show();

                                    }
                                }
                                else if(id==R.id.speaker)
                                {
                                    if(sharedPreferences.getBoolean(myid+listItem.getUser_id()+"speaker",true)){
                                        editor.putBoolean(myid+listItem.getUser_id()+"speaker",false);
                                        editor.commit();
                                        Toast.makeText(context, "Speaker off", Toast.LENGTH_SHORT).show();

                                    }else {
                                        editor.putBoolean(myid+listItem.getUser_id()+"speaker",true);


                                        editor.commit();
                                        Toast.makeText(context, "Speaker on", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else if(id==R.id.removefavorite){
                                    removeFavourite(myid,listItem.getUser_id());
                                }

                                return false;
                            }
                        });
                    }
                });
        }





    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class My_holder extends RecyclerView.ViewHolder{


        public TextView username ;
        public ImageView userimage ;
        public ImageButton option;

        public My_holder(View itemView) {
            super(itemView);

            username =itemView.findViewById(R.id.username);
            userimage =itemView.findViewById(R.id.userimage);
            option =itemView.findViewById(R.id.option);

        }
        public void bind(final ListItem item, final FavoriteList_Adapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);

                }
            });
        }
    }

    public void removeFavourite(final String user_id, final String traget_id){

        sharedPreferences.edit().putBoolean(traget_id+"favorite",false).commit();
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.remove_favorite,
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response) {
                     listener.ReloadData();
                        People.isfavoriteAdd_forpeople=true;
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("target_id", traget_id);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(postRequest);

    }


}

