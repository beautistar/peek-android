package com.calling.peek.Calling;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.calling.peek.Constant;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.calling.CallListener;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by AQEEL on 4/29/2018.
 */

//public class CallingService extends Service implements CallClientListener, CallListener {
public class CallingService extends Service implements CallClientListener, CallListener {

    //todo waheedsabir
    AudioManager audioManager;

    private static CallingService serviceobj = null;

//    private static final String APP_KEY = "413b062c-a6b3-4c9f-a298-93305aeb159b";
//    private static final String APP_SECRET = "noudCHtzq0Cv+swRmq8cCg==";
//    private static final String ENVIRONMENT = "sandbox.sinch.com";

    // live keys

    private static final String APP_KEY = "1b1af664-a0f5-45f8-9bd3-2b3a75751224";
    private static final String APP_SECRET = "DK2coP6RhU+ilu6KkuoZgA==";
    private static final String ENVIRONMENT = "sandbox.sinch.com";


    private Call call;
    private SinchClient sinchClient;

    Uri notification;
    Ringtone r;

    SharedPreferences sharedPreferences;


    public static boolean iscallrunning = false;

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public CallingService getService() {
            return CallingService.this;
        }
    }

    boolean mAllowRebind;
    private ServiceCallback serviceCallbacks;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = this.getSharedPreferences(Constant.spref, MODE_PRIVATE);

        notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        r = RingtoneManager.getRingtone(this, notification);



        serviceobj = this;
        Createcall_listner(sharedPreferences.getString("user_id", "000"));

        //notification code
        Intent notificationIntent = new Intent(this, this.getClass());

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_main)
                .setContentTitle("Peek")
                .setContentText("Connected")
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        serviceobj = this;
//        Createcall_listner(sharedPreferences.getString("user_id", "000"));


        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);


        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);

    }

    public void setCallbacks(ServiceCallback callbacks) {
        serviceCallbacks = callbacks;
    }


    public void Createcall_listner(String callerId) {

        sinchClient = Sinch.getSinchClientBuilder()
                .context(this)
                .userId(callerId)
                .applicationKey(APP_KEY)
                .applicationSecret(APP_SECRET)
                .environmentHost(ENVIRONMENT)
                .build();
        sinchClient.setSupportCalling(true);
        sinchClient.startListeningOnActiveConnection();
        sinchClient.start();
        sinchClient.getCallClient().addCallClientListener(serviceobj);

    }


    public void Do_call(String receipt_id) {
        call = null;
        call = sinchClient.getCallClient().callUser(receipt_id);
        call.addCallListener(serviceobj);
        if (serviceCallbacks != null) {
            serviceCallbacks.ShowStatus("Hang Up");
        }
        stoptimer();

    }

    public void EndCall() {
        if (r.isPlaying()) {
            r.stop();
        }
        if (call != null) {
            call.hangup();
        }

    }


    public void answercall() {
        if (call != null) {
            call.answer();
            call.addCallListener(serviceobj);
            r.stop();
            if (serviceCallbacks != null) {
                serviceCallbacks.ShowStatus("Hang Up");
            }
        }
    }

    //Callclient Listener
    @Override
    public void onIncomingCall(CallClient callClient, Call incomingCall) {
        call = incomingCall;
//        incomingCall.answer();
//        //TODO waheedsabir mic
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        Log.e("comming call==", "comming");


        //  audioManager.setMicrophoneMute(true);
        //  micbtn.setImageResource(R.drawable.mic);

        call = incomingCall;
        //check user is blocked or not
        if (!sharedPreferences.getBoolean(incomingCall.getRemoteUserId(), false)) {

            if (sharedPreferences.getBoolean(Constant.answer_for_all, false)) {
                call.answer();
                call.addCallListener(serviceobj);
                Intent intent = new Intent();
                intent.setAction("callBroadcat");
                intent.putExtra("receipt_id", incomingCall.getRemoteUserId());
                intent.putExtra("connectstatus", "Connected");
                intent.putExtra("Buttonstatus", "Hang up");
                serviceobj.sendBroadcast(intent);
                playbeepsound();

            } else if (sharedPreferences.getBoolean(incomingCall.getRemoteUserId() + "favorite", false)) {
                //wake scree auto connect
                PowerManager.WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
                        PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
                screenLock.acquire();
                screenLock.release();
                ///


                call.answer();
                call.addCallListener(serviceobj);
                Intent intent = new Intent();
                intent.setAction("callBroadcat");
                intent.putExtra("receipt_id", incomingCall.getRemoteUserId());
                intent.putExtra("connectstatus", "Connected");
                intent.putExtra("Buttonstatus", "Hang up");
                serviceobj.sendBroadcast(intent);
                playbeepsound();
            } else {
                r.play();
                Intent intent = new Intent();
                intent.setAction("callBroadcat");
                intent.putExtra("receipt_id", incomingCall.getRemoteUserId());
                intent.putExtra("connectstatus", "calling you");
                intent.putExtra("Buttonstatus", "Answer");

                serviceobj.sendBroadcast(intent);
            }
        } else {
            if (call != null) {
                call.hangup();
            }
        }

    }

    //calllistner method
    @Override
    public void onCallEnded(Call endedCall) {
        call = null;

        SinchError a = endedCall.getDetails().getError();
        if (r.isPlaying()) {
            r.stop();
        }
        iscallrunning = false;
        Cancelnoti();
        stoptimer();
        if (serviceCallbacks != null) {
            serviceCallbacks.ShowStatus("Call");
        }

        /// Todo: finish activity

        Intent intent = new Intent();
        intent.setAction("callBroadcat");
        intent.putExtra("receipt_id", endedCall.getRemoteUserId());
        intent.putExtra("connectstatus", "call_ended");
        intent.putExtra("Buttonstatus", "Answer");

        serviceobj.sendBroadcast(intent);

    }


    @Override
    public void onCallEstablished(Call establishedCall) {
        if (r.isPlaying()) {
            r.stop();
        }
        makeNotification();
        iscallrunning = true;


        startTimer();

        if (serviceCallbacks != null) {
            serviceCallbacks.ShowStatus("Connected");
        }


    }

    @Override
    public void onCallProgressing(Call progressingCall) {

        if (serviceCallbacks != null) {
            serviceCallbacks.ShowStatus("ringing");
        }
    }


    @Override
    public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
    }


    public CountDownTimer timer;

    public void startTimer() {
        //todo waheedsabir
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(16069000, 1000) { // adjust the milli seconds here
            int hour = 0, second = 0;

            public void onTick(long millisUntilFinished) {
                second = second + 1;
                if (second == 60) {
                    hour = hour + 1;
                    second = 0;
                }
                if (serviceCallbacks != null) {

                    serviceCallbacks.ShowTime("" + hour + ":" + second);
                }
            }

            public void onFinish() {
            }
        }.start();

    }

    public void stoptimer() {
        if (timer != null)
            timer.cancel();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent("startserviceonstop");
        serviceobj.sendBroadcast(intent);
    }


    NotificationManager mNotificationManager;

    private void makeNotification() {

        Notification noti = new Notification.Builder(this)
                .setContentTitle("Peek Calling")
                .setContentText("Your Calling is On.....")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setOngoing(true) // Again, THIS is the important line
                .build();
        noti.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(123, noti);
    }

    public void Cancelnoti() {
        if (mNotificationManager != null)
            mNotificationManager.cancel(123);
    }


    public void playbeepsound() {
        final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.beep_sound);
        mp2.setVolume(1.0f, 1.0f);
        mp2.start();
        mp2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp2.stop();
                mp2.release();
            }
        });

    }


}
