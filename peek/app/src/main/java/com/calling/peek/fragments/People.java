package com.calling.peek.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Adapter.ListItem;
import com.calling.peek.Adapter.My_Adpater;
import com.calling.peek.Constant;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class People extends Fragment {

    RecyclerView.Adapter adapter ;
    private List<ListItem> listItems;

    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor ed ;

    RecyclerView recyclerView ;

   public static boolean  isfavoriteAdd_forpeople=false;

    public People() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_people, container, false);


        sharedPreferences=getContext().getSharedPreferences(Constant.spref, Context.MODE_PRIVATE);

        recyclerView  = v.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));

        load();

        return v;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(isfavoriteAdd_forpeople){
                load();
                isfavoriteAdd_forpeople=false;
            }
        }
    }

    public void load (){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.Contectlist,
                new Response.Listener<String>()
                {
                    String change ;
                    @Override
                    public void onResponse(String response) {
                        Log.d("resp",response);
                        listItems = new ArrayList<>();
                        try {
                            JSONObject  jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("contact_list");

                            for (int i = 0 ; i<jsonObject.getJSONArray("contact_list").length();i++){

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                change =   jsonObject1.getString("photo_url");

                                ListItem list_items = new ListItem(
                                        jsonObject1.getString("user_id"),
                                        jsonObject1.getString("email"),
                                        jsonObject1.getString("user_name"),
                                        jsonObject1.getString("phone_number"),
                                        change,
                                        jsonObject1.getString("status")
                                        );

                                listItems.add(list_items);

                            }

                            adapter= new My_Adpater(listItems, getContext(), new My_Adpater.OnItemClickListener() {
                                @Override
                                public void onItemClick(ListItem item) {

                                }
                                public void ReloadData(){
                                    load();
                                }
                            });

                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(getActivity(), "Network Problem", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getString("user_id","0"));


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue =Volley.newRequestQueue(getActivity());
        requestQueue.add(postRequest);

    }

    @Override
    public void onResume() {
        super.onResume();

        load();
    }
}
