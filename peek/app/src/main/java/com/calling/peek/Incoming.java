package com.calling.peek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.calling.peek.Calling.Calling;
import com.waheedsabir.hclient_projectauto_call_answer.R;

public class Incoming extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming);

    }

    public void receive(View view) {


        startActivity(new Intent(Incoming.this, Calling.class));
    }
}

