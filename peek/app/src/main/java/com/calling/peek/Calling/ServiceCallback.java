package com.calling.peek.Calling;

/**
 * Created by AQEEL on 4/29/2018.
 */

public interface ServiceCallback {
    void ShowStatus(String status);
    void ShowTime(String timer);
}
