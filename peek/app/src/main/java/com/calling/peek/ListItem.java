package com.calling.peek;

public class ListItem {

    String title ;
    int  image ;

    public ListItem(String title, int image) {
        this.title = title;
        this.image = image;
    }


    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }
}
