package com.calling.peek.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.calling.peek.Calling.Calling;
import com.calling.peek.Constant;
import com.calling.peek.fragments.Favorite;
import com.calling.peek.fragments.People;
import com.squareup.picasso.Picasso;
import com.waheedsabir.hclient_projectauto_call_answer.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class My_Adpater extends RecyclerView.Adapter<My_Adpater.My_holder> {
    private List<ListItem> listItems ;
    private Context context ;

    private OnItemClickListener listener;

    SharedPreferences sharedPreferences;

    public interface OnItemClickListener {
        void onItemClick(ListItem item);
        void ReloadData();
    }

    public My_Adpater(List<ListItem> listItems, Context context,OnItemClickListener listener) {
        this.listItems =listItems ;
        this.context =context ;
        this.listener=listener;
        sharedPreferences=context.getSharedPreferences(Constant.spref,Context.MODE_PRIVATE);
    }


    @NonNull
    @Override
    public My_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_item,parent,false);

        return new My_holder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull final My_holder holder, int position) {


        final ListItem listItem =listItems.get(position);
        holder.textView.setText(listItem.user_name);
        holder.bind(listItems.get(position), listener);

        if(listItem.photo_url.equals("")){
            Picasso.get()
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imageView);
        }else {
            Picasso.get()
                    .load(listItem.photo_url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imageView);

        }


        if(listItem.getStatus().equals("Accepted")){
            holder.p_f_layout.setVisibility(View.VISIBLE);
            holder.acceptbtn.setVisibility(View.GONE);

            holder.callbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, Calling.class);
                    intent.putExtra("receipt_id",listItem.getUser_id());
                    context.startActivity(intent);
                }
            });

            holder.favbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 Addfavorite(sharedPreferences.getString("user_id","000"),listItem.getUser_id());
                }
            });

        }else {
            holder.p_f_layout.setVisibility(View.GONE);
            holder.acceptbtn.setVisibility(View.VISIBLE);

            holder.acceptbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Acceptuser(sharedPreferences.getString("user_id","000"),listItem.getUser_id());
                    holder.p_f_layout.setVisibility(View.VISIBLE);
                    holder.acceptbtn.setVisibility(View.GONE);


                }
            });
        }

        }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class My_holder extends RecyclerView.ViewHolder{


        public TextView textView ;
        public ImageView imageView ;
        public ImageButton callbtn;
        public ImageButton favbtn;
        public LinearLayout p_f_layout;
        public Button acceptbtn;

        public My_holder(View itemView) {
            super(itemView);

            textView =itemView.findViewById(R.id.text);
            imageView =itemView.findViewById(R.id.image);
            callbtn =itemView.findViewById(R.id.phonebtn);
            favbtn =itemView.findViewById(R.id.favoritebtn);
            p_f_layout=itemView.findViewById(R.id.p_f_layout);
            acceptbtn=itemView.findViewById(R.id.accept_btn);

        }
        public void bind(final ListItem item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
    }
}


    public void Acceptuser(final String senderid, final String receiverid){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.AcceptInvite,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("req",response);
                        Toast.makeText(context, "User Add in your Contact list", Toast.LENGTH_SHORT).show();

                        //todo waheedsabir
                        listener.ReloadData();
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", senderid);
                params.put("target_id", receiverid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(postRequest);

    }

    public void Addfavorite(final String senderid, final String receiverid){
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.addfavorite,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        Log.d("req",response);
                        Favorite.isfavoriteAdd=true;
                        People.isfavoriteAdd_forpeople=true;
                        sharedPreferences.edit().putBoolean(receiverid+"favorite",true).commit();
                        Toast.makeText(context, "Add in favorite list", Toast.LENGTH_SHORT).show();

                        listener.ReloadData();



                        //todo waheedsabir
                      notifyDataSetChanged();


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", ""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", senderid);
                params.put("target_id", receiverid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String,String> map = new HashMap<>(); map.put("Content-Type","multipart/form-data");
                return super.getHeaders();
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(postRequest);
    }


}
